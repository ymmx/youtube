# -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import
import numpy as np
from tkinter import *
import random
import colorsys
from PIL import Image


from PIL import ImageGrab

def colorT(c):
    return '#%02x%02x%02x' % tuple(c)

class pendulum:
    def __init__(self,d,alpha,x0,y0,c):
        self.fix_x = x0
        self.fix_y = y0
        self.length = d
        self.angle = [alpha, alpha]
        self.pos = np.zeros(2)
        self.getpos()

        self.m=1000.
        self.g=9.81
        self.damp = 0.999
        self.dt=0.01
        self.color=c
        self.cv_dot=None
        self.cv_lin=None

    def getpos(self):
        self.pos[0] =  self.length*np.sin(self.angle[0])+self.fix_x
        self.pos[1] =  self.length*np.cos(self.angle[0]) +self.fix_y

    def newAngle(self):
        self.angle[1] +=  self.dt *(-self.m*self.g/self.length*np.sin(self.angle[0]))
        self.angle[0] += self.angle[1]*self.dt

class MyApp(Tk):
    def __init__(self):
        self.nbi=0


        Tk.__init__(self)
        fr = Frame(self)
        fr.pack()
        width =np.int(1288)
        height = np.int(728)
        self.canvas  = Canvas(fr, height = height, width = width)#,bg= 'black')
        self.canvas.pack()
        self.w = int(width/10)
        self.h = int(height/10)
        self.Matpoint = np.zeros((self.h,self.w),dtype=np.bool_)
        # self.Matpoint =np.random.choice([False, True], size=(int(self.h),int(self.w)), p=[9./10, 1./10])
        # self.Matpoint[h/2,w/2]=True
        self.scale = width/float(self.w)
        if self.scale > height/float(self.h):
            self.scale = height/float(self.h)
        self.ant = [self.h//2,self.w//2]
        self.antgo = 'u'

        o=self.canvas.create_rectangle(0, 0,width,height)
        self.canvas.itemconfig(o, fill="black")
        self.circle=self.Matpoint.tolist()


        for i in range(self.Matpoint.shape[0]):
            for j in range(self.Matpoint.shape[1]):
                self.circle[i][j]=self.canvas.create_oval(j*self.scale, i*self.scale,j*self.scale+self.scale, i*self.scale+self.scale)
                if self.Matpoint[i,j]:
                    self.canvas.itemconfig(self.circle[i][j], fill="blue")
                else:
                    self.canvas.itemconfig(self.circle[i][j], fill="black")
            # self.canvas.itemconfig(self.p[i].cv_dot, outline=self.p[i].color)
            # self.canvas.itemconfig(self.p[i].cv_dot, fill=self.p[i].color)
        self.canvas.update()
        #self.update()
        self.end =True

    def update(self ):
        for k in range(1):
            self.Matpoint[self.ant[0],self.ant[1]] = not self.Matpoint[self.ant[0],self.ant[1]]
            if self.Matpoint[self.ant[0],self.ant[1]]:
                self.canvas.itemconfig(self.circle[self.ant[0]][self.ant[1]], fill="blue")
            else:
                self.canvas.itemconfig(self.circle[self.ant[0]][self.ant[1]], fill="black")

            print(self.nbi )
            if self.antgo =='u':
                self.ant[0] = self.ant[0]+1
            elif self.antgo =='d':
                self.ant[0] = self.ant[0]-1
            elif self.antgo =='l':
                self.ant[1] = self.ant[1]-1
            elif self.antgo =='r':
                self.ant[1] = self.ant[1]+1

            if self.ant[0]<0 or self.ant[0]>self.h:
                quit()
            if self.ant[1]<0 or self.ant[1]>self.w:
                quit()
            print(self.ant[0],self.ant[1],self.Matpoint[self.ant[0],self.ant[1]],self.antgo )
            if self.Matpoint[self.ant[0],self.ant[1]]==True:
                if self.antgo =='u':
                   self.antgo =  'r'
                elif self.antgo =='d':
                    self.antgo = 'l'
                elif self.antgo =='l':
                    self.antgo = 'u'
                elif self.antgo =='r':
                    self.antgo =  'd'
            else:
                if self.antgo =='u':
                   self.antgo =  'l'
                elif self.antgo =='d':
                    self.antgo = 'r'
                elif self.antgo =='l':
                    self.antgo = 'd'
                elif self.antgo =='r':
                    self.antgo =  'u'

            if self.ant[0]<0 or self.ant[0]>self.h:
                self.end==False
                return

            if self.ant[1]<0 or self.ant[1]>self.w:
                self.end==False
                return


        # self.canvas.delete('all')

        # for i in range(self.Matpoint.shape[0]):
        #     for j in range(self.Matpoint.shape[1]):
        #         if self.Matpoint[i,j]:
        #             o=self.canvas.create_oval(j*self.scale-self.scale/2., i*self.scale-self.scale/2.,j*self.scale+self.scale/2., i*self.scale+self.scale/2.)
        #             self.canvas.itemconfig(o, fill="blue")

        self.canvas.update()
        if 1:
            x= self.canvas.winfo_rootx()
            y= self.canvas.winfo_rooty()
            # print(x,y,self.canvas.winfo_width(),self.canvas.winfo_height())
            # self.canvas.postscript(file='Rain'  + str(self.nbi).zfill(5)+'.ps', colormode='color')
            file='C:\\Users\\maxime\\Desktop\\dd\\ant'  + str(self.nbi).zfill(6)+'.png'
            ImageGrab.grab((x+4,y+4,x+4+1280,y+4+720)).save(file)

            # self.canvas.postscript(file=file, colormode='color')
            # img = Image.open(file)
            #
            # img.save(file[:-3]+'.png', "png",quality=99)
        self.nbi+=1

        return
if __name__ == "__main__":
    root = MyApp()
    i=0
    while  root.end:
        root.update( )
        root.after(1)