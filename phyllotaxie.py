# -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import
from tkinter import *
import numpy as np 
import colorsys

def colorT(c):
    return '#%02x%02x%02x' % tuple(c)

class MyApp(Tk):
    def __init__(self):
        self.nbi=0
        self.d = 2.
        self.c = 10.
        self.nb = 1000
        self.n = np.arange(self.nb)
        self.angle=153.
        self.r=np.zeros(self.nb)
        self.phi=np.zeros(self.nb)
        self.x=np.zeros(self.nb)
        self.y=np.zeros(self.nb)
        self.cv = range(self.nb)
        self.color=[colorT((255,255,255))] * self.nb

        Tk.__init__(self)
        fr = Frame(self)
        fr.pack()
        width = 1288
        height = 728
        self.canvas  = Canvas(fr, height = height, width = width,bg= 'black')
        self.canvas.pack()
        for i in self.n:
            self.cv[i] =self.canvas.create_oval(self.x[i]-self.d, self.y[i]-self.d,self.x[i]+self.d, self.y[i]+self.d )
        self.update()
        for i in self.n:
            dist= ((self.x[i] - width/2.)**2 + (self.y[i] - height/2.)**2)**.5 / (height/2.)
            h=colorsys.hsv_to_rgb(dist, 1., 1.)
            self.color[i] =colorT((h[0]*255,h[1]*255,h[2]*255))
            self.canvas.itemconfig(self.cv[i], outline=self.color[i])
            self.canvas.itemconfig(self.cv[i], fill=self.color[i])



    def update(self ):
        self.angle +=0.00001

        self.phi = self.angle * self.n
        self.r = self.c * np.sqrt(self.n)
        self.x = self.r * np.cos(self.phi) +self.canvas.winfo_reqwidth()/2.
        self.y = self.r * np.sin(self.phi) +self.canvas.winfo_reqheight()/2.
        for i in self.n:
            self.canvas.coords(self.cv[i],self.x[i]-self.d, self.y[i]-self.d,self.x[i]+self.d, self.y[i]+self.d )

        self.canvas.update()

        return
if __name__ == "__main__":
    root = MyApp()
    i=0
    while 1:
        root.update( )
        root.after(1)